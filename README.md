
# Arch Basis-Installationsbefehl-Skript

  

In diesem Repository findest du Pakete und Skripte für die Basisinstallation von Arch Linux und den Desktop-Umgebungen Gnome, KDE, Cinnamon und Xfce, sowie diverser Fenstermanager.

Ändere die Pakete nach deinem Geschmack, mache das Skript mit chmod +x Skriptname ausführbar und führe es dann mit ./scriptname aus.

Bedenke, dass der erste Teil der Arch Linux Installation manuell ist, d.h. du musst die Festplatte selbst partitionieren, formatieren und mounten. Du installierst die Basispakete mit Pacstrap und stellst sicher, dass du git einbindest, damit du das Repository in chroot klonen kannst.

  

#### Eine kleine Zusammenfassung:
1. Falls erforderlich, lade deine Tastaturbelegung
```
loadkeys de-latin1
```
2. Aktualisiere die Server mit
```
pacman -Sy
```
3. Partitioniere die Platte
4. Formatiere die Partitionen
5. Einhängen der Partitionen
6. Installiere die Basispakete nach /mnt 

Bei Intel: 
```
pacstrap /mnt base linux linux-firmware git vim intel-ucode
```
Bei AMD:
```
pacstrap /mnt base linux linux-firmware git vim amd-ucode
```
8. Erstelle die FSTAB-Datei mit
```
genfstab -U /mnt >> /mnt/etc/fstab
```
8. Chroot mit
```
arch-chroot /mnt
```
9. Hol dir das Git-Repository mit
```
git clone https://gitlab.com/sontypiminternet/arch-basic
```
10. Wechsle ins Verzeichnis "arch-base"
```
cd arch-basic
```
11. Mach die Datei ausführbar
```
chmod +x base-uefi.sh
```
oder
```
chmod +x base-mbr.sh
```
12. Ausführen mit
```
./base-uefi.sh 
```
oder
```
./base-mbr.sh 
```
